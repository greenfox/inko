tool
extends Node

var Inks = ["RED","YELLOW","BLUE","WHITE","BLACK"]

var RED
var YELLOW
var BLUE
var WHITE
var BLACK

var PhyLayers = {}

var playerColor = null

func _ready():
	for i in range(1, 21):
		var layer_name = ProjectSettings.get_setting(str("layer_names/2d_physics/layer_", i))
		if( layer_name ):
			PhyLayers[layer_name] = int(pow(2,i-1))
	RED = {
		"name":"RED",
		"phy":PhyLayers.RED,
		"color":Color(1,0,0)
	}
	YELLOW = {
		"name":"YELLOW",
		"phy":PhyLayers.YELLOW,
		"color":Color(1,1,0)
	}
	BLUE = {
		"name":"BLUE",
		"phy":PhyLayers.BLUE,
		"color":Color(0,0,1)
	}
	WHITE = {
		"name":"WHITE",
		"phy":PhyLayers.BLUE | PhyLayers.YELLOW | PhyLayers.RED,
		"color":Color(1,1,1)
	}
	BLACK = {
		"name":"BLACK",
		"phy":0,
		"color":Color(0.2,0.2,0.2)
	}
	print('hello')
 